;;; srfi64-test.el --- Tests for srfi64.el. -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Jérémy Korwin-Zmijowski

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;; See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <https://www.gnu.org/licenses/>.

;; Author: Jérémy Korwin-Zmijowski <jeremy@korwin-zmijowski.fr>
;; Maintainer: Jérémy Korwin-Zmijowski <jeremy@korwin-zmijowski.fr>
;; Keywords: scheme srfi64 tools testing
;; Version: 0.0.1
;; Homepage: https://framagit.org/Jeko/emacs-srfi64
;; Package-Requires: ((emacs "25.1") (geiser "0.27"))

;;; Commentary:

;; Here you can find the tests written to drive the development of
;; srfi64.el.
;; As a contribution guidelines, one can :
;; - add a test and see it failing
;; - add code to make the test to pass
;; - refactor the code base
;; Run the following shell command to run these tests :
;;
;; `emacs -batch -l ert -l srfi64.el -l srfi64-test.el -f ert-run-tests-batch-and-exit`

;;; Code:

(load-file "srfi64.el")
(defalias 'srfi64-eval-region 'geiser-eval-region/wait)

(require 'f)
(require 'geiser-mode)

(defvar srfi64-test--test-runner-log-file "/tmp/srfi64-test.log")

(defun perform-on (context actions)
  "Perform ACTIONS on CONTEXT."

  (defun install-srfi64-test-runner (tests)
    "Insert scheme code."
    (insert
     (format "
(use-modules (ice-9 format)
             (srfi srfi-64))

(define (srfi64-test-runner filename)
  (let ((runner (test-runner-null))
	(port (open-output-file filename))
	(num-run 0))
    (test-runner-on-test-end! runner
			      (lambda (runner)
				(case (test-result-kind runner)
				  ((pass xpass) (set! num-run (+ num-run 1)))
				  (else #t))))
    (test-runner-on-final! runner
			   (lambda (runner)
			     (format port \"~d\" num-run)
			     (close-output-port port)))
    runner))

(test-runner-factory
 (lambda ()
   (srfi64-test-runner \"%s\")))
" srfi64-test--test-runner-log-file)))

  (defun prettify-buffer ()
    (indent-region (point-min) (point-max)))

  (geiser-guile)
  (with-temp-buffer
    (scheme-mode)
    (install-srfi64-test-runner context)
    (geiser-eval-region/wait (point-min) (point-max))
    (insert context)
    (prettify-buffer)
    (write-file "/tmp/srfi64-temp-buffer.scm")
    (funcall actions)))

(defun summary ()
  "Return summary from TEST_RUNNER_LOG_FILE."
  (with-temp-buffer
    (erase-buffer)
    (insert-file-contents srfi64-test--test-runner-log-file)
    (delete-file srfi64-test--test-runner-log-file)
    (string-to-number (buffer-string))))

(ert-deftest run-all-buffer-one-suite-one-test ()
  "Given one suite with one test.  When evaluating the whole buffer.  Then one test has been run."
  (let ((context "\n(test-begin \"dummy-suite\")\n(test-assert #t)\n(test-end \"dummy-suite\")")
	(actions (lambda () (srfi64-run-all-buffer))))
    (perform-on context actions)
    (should (equal 1 (summary)))))

(ert-deftest run-suite-at-point-nested-suite-innest ()
  "Given two nested suites with two tests per suite.  When evaluating the suite at point (inner one).  Then two tests have been run."
  (let ((context "\n(test-begin \"dummy-suite-1\")\n(test-assert \"dummy-test-1-1\" #t)\n(test-assert #t)\n(test-assert #t)\n(test-begin \"dummy-suite-2\")\n(test-assert \"dummy-test-2-1\" #t)\n(test-assert #t)\n(test-end \"dummy-suite-2\")\n(test-end \"dummy-suite-1\")")
	(actions (lambda () (search-backward "dummy-test-2-1" nil t) (srfi64-run-test-suite-at-point))))
    (perform-on context actions)
    (should (equal 2 (summary)))))

(ert-deftest run-test-at-point ()
  "Given one test in one suite.  When run test at point.  Then one test have been run."
  (let ((context "\n(test-begin \"dummy-suite-1\")\n(test-assert \"dummy-test-1-1\" #t)\n(testst-end \"dummy-suite-1\")")
	(actions (lambda () (search-backward "dummy-test-1" nil t) (srfi64-run-test-suite-at-point))))
    (perform-on context actions)
    (should (equal 4 (summary)))))

(provide'srfi64-test)
;;; srfi64-test.el ends here
