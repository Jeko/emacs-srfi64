;;; srfi64.el --- SRFI 64 tools for Emacs. -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Jérémy Korwin-Zmijowski

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;; See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <https://www.gnu.org/licenses/>.

;; Author: Jérémy Korwin-Zmijowski <jeremy@korwin-zmijowski.fr>
;; Maintainer: Jérémy Korwin-Zmijowski <jeremy@korwin-zmijowski.fr>
;; Keywords: scheme srfi64 tools testing
;; Version: 0.0.1
;; Homepage: https://framagit.org/Jeko/emacs-srfi64
;; Package-Requires: ((emacs "25.1") (geiser "0.27"))

;;; Commentary:

;; srfi64.el is a minor mode to provide facilities to run tests
;; in Emacs.

;;; Code:

(require 'geiser-mode)

;; geiser-eval-region is asynchronous
;; in tests, I use the synchrounous geiser-eval-region/wait
(defalias 'srfi64-eval-region 'geiser-eval-region)

(defun srfi64-run-all-buffer ()
  "Run the whole buffer."
  (srfi64-eval-region (point-min) (point-max)))

(defun srfi64-run-test-suite-at-point ()
  "Run test suite at point."
  (let ((suite-start (save-excursion
		       (search-backward "test-begin" nil t)
		       (line-beginning-position)))
	(suite-end (save-excursion
		     (search-forward "test-end" nil t)
		     (line-end-position))))
    (srfi64-eval-region suite-start suite-end)))

(define-minor-mode srfi64-mode
  "Toggle SRFI 64 mode."
  :init-value nil
  :lighter " srfi64"
  :keymap
  (list
   (cons (kbd "C-c r a") (lambda () (interactive) (srfi64-run-all-buffer)))
   (cons (kbd "C-c r s") (lambda () (interactive) (srfi64-run-test-suite-at-point)))))

(provide 'srfi64)
;;; srfi64.el ends here
